package com.plugsurfing.prototype

import com.plugsurfing.prototype.api.v1.LocationPut
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class LocationController {

    @PutMapping("/locations")
    fun save(@Valid @RequestBody location: LocationPut) = location

}
