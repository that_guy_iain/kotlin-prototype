package com.plugsurfing.prototype

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.slf4j.LoggerFactory
import org.springframework.core.codec.CodecException
import org.springframework.core.codec.DecodingException
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR
import org.springframework.http.ResponseEntity
import org.springframework.kafka.core.KafkaProducerException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.support.WebExchangeBindException
import org.springframework.web.server.ServerWebInputException
import java.util.concurrent.ExecutionException


data class ErrorResponse (
    @get:JsonProperty("status_code") val code: Int,
    @get:JsonProperty("status_message") val message: String,
    @get:JsonProperty("details") val details: List<String>? = null
)

@ControllerAdvice
class RestExceptionHandler {
    companion object {
        private val LOG = LoggerFactory.getLogger(RestExceptionHandler::class.java)
        private const val REQUEST_NOT_VALID_MSG = "The request is not valid"
    }

    @ExceptionHandler(value = [Exception::class])
    fun handle(e: Throwable?): ResponseEntity<ErrorResponse> {
        return when (e) {
            is WebExchangeBindException -> handleValidationException(e)
            is ServerWebInputException -> e.cause?.let { handleParsingException(e.cause) }
                ?: run { handleSpringValidationException(e) }
            is MethodArgumentNotValidException -> handleValidationException(e)
            is KafkaProducerException -> handleKafkaException(e)
            is ExecutionException -> handle(e.cause)
            is CodecException -> handleParsingException(e.cause)
            is InvalidDefinitionException -> handleParsingException(e)
            else -> {
                LOG.error(e?.message, e)
                ErrorResponse(INTERNAL_SERVER_ERROR.value(), "Unexpected Server Error").buildResponseEntity()
            }
        }
    }

    private fun handleSpringValidationException(e: ServerWebInputException): ResponseEntity<ErrorResponse> {
        return ErrorResponse(BAD_REQUEST.value(), REQUEST_NOT_VALID_MSG, listOf(e.reason)).buildResponseEntity()
    }
    private fun handleValidationException(e: MethodArgumentNotValidException): ResponseEntity<ErrorResponse> {
        LOG.info(e.message)
        return ErrorResponse(BAD_REQUEST.value(), REQUEST_NOT_VALID_MSG, e.bindingResult.fieldErrors
            .mapNotNull { "${it.field} value [${it.rejectedValue}] is not valid: ${it.defaultMessage}" }).buildResponseEntity()
    }

    private fun handleKafkaException(e: KafkaProducerException): ResponseEntity<ErrorResponse> {
        LOG.error("Couldn't post to kafka", e)
        return ErrorResponse(INTERNAL_SERVER_ERROR.value(), INTERNAL_SERVER_ERROR.reasonPhrase).buildResponseEntity()
    }

    private fun handleValidationException(e: WebExchangeBindException): ResponseEntity<ErrorResponse> {
        val errors = e.bindingResult.fieldErrors
            .mapNotNull { "${it.field} value [${it.rejectedValue}] is not valid: ${it.defaultMessage}" }
        return ErrorResponse(BAD_REQUEST.value(), REQUEST_NOT_VALID_MSG, errors).buildResponseEntity()
    }

    private fun handleJacksonException(e: JsonMappingException): ResponseEntity<ErrorResponse> {
        val path = getPropertyPath(e)
        return when (e) {
            is MissingKotlinParameterException -> badRequest("Property %s is missing", path)
            is InvalidFormatException -> badRequest("Value of property %s is not allowed", path)
            is UnrecognizedPropertyException -> badRequest("Property %s is not allowed", path)
            is MismatchedInputException -> badRequest("Property %s has wrong type (list instead of value?)", path)
            is InvalidDefinitionException ->  badRequest("Property %s is not valid", path)
            else -> {
                LOG.error(e.message)
                ErrorResponse(INTERNAL_SERVER_ERROR.value(), "Unexpected Server Error").buildResponseEntity()
            }
        }
    }

    private fun badRequest(pattern: String, path: String): ResponseEntity<ErrorResponse> {
        return ErrorResponse(BAD_REQUEST.value(), REQUEST_NOT_VALID_MSG, listOf(pattern.format(path))).buildResponseEntity()
    }

    private fun getPropertyPath(e: JsonMappingException): String {
        return e.path
            .map { it.fieldName ?: it.index }
            .joinToString("/")
    }

    private fun handleParsingException(e: Throwable?): ResponseEntity<ErrorResponse> {
        return when (e) {
            is WebExchangeBindException -> handleParsingException(e.cause)
            is DecodingException -> handleParsingException(e.cause)
            is JsonMappingException -> handleJacksonException(e)
            is JsonParseException -> ErrorResponse(BAD_REQUEST.value(), REQUEST_NOT_VALID_MSG, listOf("Unparsable JSON")).buildResponseEntity()
            else -> {
                LOG.error(e?.message)
                ErrorResponse(INTERNAL_SERVER_ERROR.value(), "Unexpected Server Error").buildResponseEntity()
            }
        }
    }

    private fun ErrorResponse.buildResponseEntity(): ResponseEntity<ErrorResponse> {
        return ResponseEntity
            .status(code)
            .body(this)
    }

}
