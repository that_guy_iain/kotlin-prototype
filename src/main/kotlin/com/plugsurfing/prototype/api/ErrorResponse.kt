package com.plugsurfing.prototype.api

import com.fasterxml.jackson.annotation.JsonProperty

data class ErrorResponse (
    @get:JsonProperty("status_code") val code: Int,
    @get:JsonProperty("status_message") val message: String,
    @get:JsonProperty("details") val details: List<String>? = null
)
