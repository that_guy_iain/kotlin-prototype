package com.plugsurfing.prototype.api

class InvalidEnumOptionException(message: String?) : RuntimeException(message)
