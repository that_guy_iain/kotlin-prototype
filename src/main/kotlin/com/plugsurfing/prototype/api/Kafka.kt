package com.plugsurfing.prototype.api

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.plugsurfing.prototype.api.v1.*

interface KafkaPayload

@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS)
@JsonSubTypes(
    JsonSubTypes.Type(value = LocationPut::class),
    JsonSubTypes.Type(value = EvsePut::class),
    JsonSubTypes.Type(value = ConnectorPut::class),
    JsonSubTypes.Type(value = LocationPatch::class),
    JsonSubTypes.Type(value = EvsePatch::class),
    JsonSubTypes.Type(value = ConnectorPatch::class)
)
interface KafkaTypedPayloadMixin

data class KafkaMessage(
    val payload: KafkaPayload,
    val owner: Owner,
    val locationId: String,
    val evseId: String? = null,
    val connectorId: String? = null
)
