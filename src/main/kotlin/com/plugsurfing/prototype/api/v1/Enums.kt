package com.plugsurfing.prototype.api.v1

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue
import com.plugsurfing.prototype.api.InvalidEnumOptionException

enum class LocationType {
    ON_STREET,
    PARKING_GARAGE,
    UNDERGROUND_GARAGE,
    PARKING_LOT,
    OTHER,
    UNKNOWN
}

enum class Status {
    AVAILABLE,
    BLOCKED,
    CHARGING,
    INOPERATIVE,
    OUTOFORDER,
    PLANNED,
    REMOVED,
    RESERVED,
    OFFLINE,
    UNKNOWN,
    OCCUPIED
}

enum class Capability {
    CHARGING_PROFILE_CAPABLE,
    CREDIT_CARD_PAYABLE,
    REMOTE_START_STOP_CAPABLE,
    RESERVABLE,
    RFID_READER,
    UNLOCK_CAPABLE
}

enum class ConnectorType(val value: String, val isOcpi: Boolean = true) {
    THREE_PIN_SQUARE("3PinSquare", false),
    CEE_2_POLES("Cee2Poles", false),
    CEE_BLUE("CeeBlue", false),
    CEE_PLUS("CeePlus", false),
    CEE_RED("CeeRed", false),
    COMBO("Combo", false),
    MARECHAL("Marechal", false),
    NEMA5("Nema5", false),
    SCAME("Scame", false),
    CHADEMO_PS("Chademo", false),
    TYPE1("Type1", false),
    TYPE2("Type2", false),
    TYPE3("Type3", false),
    TYPEE("TypeE", false),
    SCHUKO("Schuko", false),
    UNKNOWN("UNKNOWN", false),
    T23("T23", false),
    TESLA("Tesla", false),
    CHADEMO("CHADEMO"),
    DOMESTIC_A("DOMESTIC_A"),
    DOMESTIC_B("DOMESTIC_B"),
    DOMESTIC_C("DOMESTIC_C"),
    DOMESTIC_D("DOMESTIC_D"),
    DOMESTIC_E("DOMESTIC_E"),
    DOMESTIC_F("DOMESTIC_F"),
    DOMESTIC_G("DOMESTIC_G"),
    DOMESTIC_H("DOMESTIC_H"),
    DOMESTIC_I("DOMESTIC_I"),
    DOMESTIC_J("DOMESTIC_J"),
    DOMESTIC_K("DOMESTIC_K"),
    DOMESTIC_L("DOMESTIC_L"),
    IEC_60309_2_SINGLE_16("IEC_60309_2_single_16"),
    IEC_60309_2_THREE_16("IEC_60309_2_three_16"),
    IEC_60309_2_THREE_32("IEC_60309_2_three_32"),
    IEC_60309_2_THREE_64("IEC_60309_2_three_64"),
    IEC_62196_T1("IEC_62196_T1"),
    IEC_62196_T1_COMBO("IEC_62196_T1_COMBO"),
    IEC_62196_T2("IEC_62196_T2"),
    IEC_62196_T2_COMBO("IEC_62196_T2_COMBO"),
    IEC_62196_T3A("IEC_62196_T3A"),
    IEC_62196_T3C("IEC_62196_T3C"),
    TESLA_R("TESLA_R"),
    TESLA_S("TESLA_S");

    companion object {
        @JsonCreator
        @JvmStatic
        fun fromValue(value: String): ConnectorType {
            return values()
                .filter { it.value == value  }
                .ifEmpty { throw InvalidEnumOptionException("$value is not a valid option") }
                .first()
        }
    }

    @JsonValue
    fun toValue() = value
}

enum class ConnectorFormat {
    SOCKET,
    CABLE
}

enum class PowerType(val isOcpi: Boolean = true) {
    AC_1_PHASE,
    AC_3_PHASE,
    DC,
    AC(false)
}

enum class ParkingRestriction {
    EV_ONLY,
    PLUGGED,
    DISABLED,
    CUSTOMERS,
    MOTORCYCLES
}

enum class ImageCategory {
    CHARGER,
    ENTRANCE,
    LOCATION,
    NETWORK,
    OPERATOR,
    OTHER,
    OWNER
}

enum class Facility {
    HOTEL,
    RESTAURANT,
    CAFE,
    MALL,
    SUPERMARKET,
    SPORT,
    RECREATION_AREA,
    NATURE,
    MUSEUM,
    BIKE_SHARING,
    BUS_STOP,
    TAXI_STAND,
    TRAM_STOP,
    METRO_STATION,
    TRAIN_STATION,
    AIRPORT,
    PARKING_LOT,
    CARPOOL_PARKING,
    FUEL_STATION,
    WIFI
}

enum class EnergySourceCategory {
    NUCLEAR,
    GENERAL_FOSSIL,
    COAL,
    GAS,
    GENERAL_GREEN,
    SOLAR,
    WIND,
    WATER
}

enum class EnvironmentalImpactCategory {
    NUCLEAR_WASTE,
    CARBON_DIOXIDE
}

enum class Owner {
    CHARGE_AND_DRIVE,
    PLUG_SURFING,
    AUTOMATION_TEST,
    LOAD_TEST
}
