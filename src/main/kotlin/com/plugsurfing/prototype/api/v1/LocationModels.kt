package com.plugsurfing.prototype.api.v1

import com.fasterxml.jackson.annotation.JsonProperty
import com.plugsurfing.prototype.api.KafkaPayload
import java.time.Instant
import javax.validation.Valid
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size


data class LocationPut(
    @field:JsonProperty("country_code") @field:Pattern(regexp = "^[A-Z]{2}", message = "country_code must be a string of 2 capital letters i.e. SE") val countryCode: String? = null,
    @field:JsonProperty("party_id") @field:Size(min = 3, max = 3) val partyId: String? = null,
    @field:Size(min = 1, max = 36) @field:NotEmpty val id: String? = null,
    val type: LocationType ?= null,
    @field:Size(min = 1, max = 255) val name: String? = null,
    @field:Size(min = 1, max = 255) val address: String? = null,
    @field:Size(min = 1, max = 255) val city: String? = null,
    @field:JsonProperty("postal_code") @field:Size(min = 1, max = 10) val postalCode: String? = null,
    @field:Size(min = 1, max = 255) val state: String? = null,
    @field:Pattern(regexp = "^[A-Z]{3}", message = "country must be a string of 3 capital letters i.e. SWE") val country: String? = null,
    @field:Valid val coordinates: GeoLocation? = null,
    @field:JsonProperty("related_locations") @field:Valid val relatedLocations: List<AdditionalGeoLocation>? = emptyList(),
    @field:Size(min = 1) @field:Valid val evses: List<EvsePut>? = null,
    @field:Valid val directions: List<DisplayText>? = emptyList(),
    @field:Valid val operator: BusinessDetails? = null,
    @field:Valid val suboperator: BusinessDetails? = null,
    @field:Valid val owner: BusinessDetails? = null,
    val facilities: List<Facility>? = emptyList(),
    @field:Size(min = 1, max = 255) @field:JsonProperty("time_zone") val timeZone: String? = null,
    @field:JsonProperty("opening_times") @field:Valid val openingTimes: Hours? = null,
    @field:JsonProperty("charging_when_closed") val chargingWhenClosed: Boolean? = null,
    @field:Valid val images: List<Image>? = emptyList(),
    @field:JsonProperty("energy_mix") @field:Valid val energyMix: EnergyMix? = null,
    @field:JsonProperty("last_updated") val lastUpdated: Instant? = null,

    // plug-surfing specific
    val notes: String? = null,
    @JsonProperty("is_roofed") @field:JsonProperty("is_roofed") val isRoofed: Boolean? = null,
    @JsonProperty("is_private") @field:JsonProperty("is_private") val isPrivate: Boolean? = null,
    @JsonProperty("total_parking") @field:JsonProperty("total_parking") val totalParking: Int? = null,
    @JsonProperty("fleet_id") @field:JsonProperty("fleet_id") val fleetId: Int? = null,
    @JsonProperty("data_source") @field:JsonProperty("data_source") val dataSource: String? = null,
    @JsonProperty("street_name") @field:JsonProperty("street_name") val streetName: String? = null,
    @JsonProperty("street_number") @field:JsonProperty("street_number") val streetNumber: String? = null,
    @JsonProperty("user_id") @field:JsonProperty("user_id") val userId: String? = null,
    @JsonProperty("deleted_at") @field:JsonProperty("deleted_at") val deletedAt: Instant? = null

) : KafkaPayload

data class EvsePut(
    val uid: String,
    @field:JsonProperty("evse_id") val evseId: String? = null,
    val status: Status,
    @field:JsonProperty("status_schedule") val statusSchedule: List<StatusSchedule>? = emptyList(),
    val capabilities: List<Capability>? = emptyList(),
    @field:Size(min = 1) @field:Valid val connectors: List<ConnectorPut>,
    @field:JsonProperty("floor_level") val floorLevel: String? = null,
    @field:Valid val coordinates: GeoLocation? = null,
    @field:JsonProperty("physical_reference") val physicalReference: String? = null,
    @field:Valid val directions: List<DisplayText>? = emptyList(),
    @field:JsonProperty("parking_restrictions") val parkingRestrictions: List<ParkingRestriction>? = emptyList(),
    @field:Valid val images: List<Image>? = emptyList(),
    @field:JsonProperty("last_updated") val lastUpdated: Instant,

    // plug-surfing specific
    @JsonProperty("is_free_charge") @field:JsonProperty("is_free_charge") val isFreeCharge: Boolean? = null,
    @JsonProperty("deleted_at") @field:JsonProperty("deleted_at") val deletedAt: Instant? = null
) : KafkaPayload

data class ConnectorPut(
    val id: String,
    @field:JsonProperty("standard") val connectorType: ConnectorType,
    @field:JsonProperty("format") val connectorFormat: ConnectorFormat? = null,
    @field:JsonProperty("power_type") val powerType: PowerType? = null,
    @field:JsonProperty("max_voltage") val maxVoltage: Int? = null,
    @field:JsonProperty("max_amperage") val maxAmperage: Int? = null,
    @field:JsonProperty("max_electric_power") val maxElectricPower: Int? = null,
    @field:JsonProperty("tariff_ids") val tariffIds: List<String>? = emptyList(),
    @field:JsonProperty("terms_and_conditions") val termsAndConditionsUrl: String? = null,
    @field:JsonProperty("last_updated") val lastUpdated: Instant
) : KafkaPayload

data class LocationPatch(
    @field:JsonProperty("country_code") @field:Pattern(regexp = "^[A-Z]{2}", message = "country_code must be a string of 2 capital letters i.e. SE") val countryCode: String? = null,
    @field:JsonProperty("party_id") val partyId: String? = null,
    val type: LocationType? = null,
    @field:Size(min = 1, max = 255) val name: String? = null,
    @field:Size(min = 1, max = 255) val address: String? = null,
    @field:Size(min = 1, max = 255) val city: String? = null,
    @field:JsonProperty("postal_code") @field:Size(min = 1, max = 10) val postalCode: String? = null,
    @field:Size(min = 1, max = 255) val state: String? = null,
    @field:Pattern(regexp = "^[A-Z]{3}", message = "country must be a string of 3 capital letters i.e. SWE") val country: String? = null,
    @field:Valid val coordinates: GeoLocation? = null,
    @field:JsonProperty("related_locations") @field:Valid val relatedLocations: List<AdditionalGeoLocation>? = null,
    @field:Valid val directions: List<DisplayText>? = null,
    @field:Valid val operator: BusinessDetails? = null,
    @field:Valid val suboperator: BusinessDetails? = null,
    @field:Valid val owner: BusinessDetails? = null,
    val facilities: List<Facility>? = null,
    @field:Size(min = 1, max = 255) @field:JsonProperty("time_zone") val timeZone: String? = null,
    @field:JsonProperty("opening_times") val openingTimes: Hours? = null,
    @field:JsonProperty("charging_when_closed") val chargingWhenClosed: Boolean? = null,
    val images: List<Image>? = null,
    @field:JsonProperty("energy_mix") val energyMix: EnergyMix? = null,

    // plug-surfing specific
    val notes: String? = null,
    @JsonProperty("is_roofed") @field:JsonProperty("is_roofed") val isRoofed: Boolean? = null,
    @JsonProperty("is_private") @field:JsonProperty("is_private") val isPrivate: Boolean? = null,
    @JsonProperty("total_parking") @field:JsonProperty("total_parking") val totalParking: Int? = null,
    @JsonProperty("fleet_id") @field:JsonProperty("fleet_id") val fleetId: Int? = null,
    @JsonProperty("data_source") @field:JsonProperty("data_source") val dataSource: String? = null,
    @JsonProperty("street_name") @field:JsonProperty("street_name") val streetName: String? = null,
    @JsonProperty("street_number") @field:JsonProperty("street_number") val streetNumber: String? = null,
    @JsonProperty("user_id") @field:JsonProperty("user_id") val userId: String? = null,
    @JsonProperty("deleted_at") @field:JsonProperty("deleted_at") val deletedAt: Instant? = null
) : KafkaPayload

data class EvsePatch(
    @field:JsonProperty("evse_id") val evseId: String? = null,
    val status: Status? = null,
    @field:JsonProperty("status_schedule") val statusSchedule: List<StatusSchedule>? = null,
    val capabilities: List<Capability>? = null,
    @field:JsonProperty("floor_level") val floorLevel: String? = null,
    val coordinates: GeoLocation? = null,
    @field:JsonProperty("physical_reference") val physicalReference: String? = null,
    val directions: List<DisplayText>? = null,
    @field:JsonProperty("parking_restrictions") val parkingRestrictions: List<ParkingRestriction>? = null,
    val images: List<Image>? = null,

    // plug-surfing specific
    @JsonProperty("is_free_charge") @field:JsonProperty("is_free_charge") val isFreeCharge: Boolean? = null,
    @JsonProperty("deleted_at") @field:JsonProperty("deleted_at") val deletedAt: Instant? = null
) : KafkaPayload

data class ConnectorPatch(
    @field:JsonProperty("standard") val connectorType: ConnectorType? = null,
    @field:JsonProperty("format") val connectorFormat: ConnectorFormat? = null,
    @field:JsonProperty("power_type") val powerType: PowerType? = null,
    @field:JsonProperty("max_voltage") val maxVoltage: Int? = null,
    @field:JsonProperty("max_amperage") val maxAmperage: Int? = null,
    @field:JsonProperty("max_electric_power") val maxElectricPower: Int? = null,
    @field:JsonProperty("tariff_ids") val tariffIds: List<String>? = null,
    @field:JsonProperty("terms_and_conditions") val termsAndConditionsUrl: String? = null
) : KafkaPayload


