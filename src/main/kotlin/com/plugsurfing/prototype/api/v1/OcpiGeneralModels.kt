package com.plugsurfing.prototype.api.v1

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal
import java.time.Instant
import javax.validation.Valid
import javax.validation.constraints.*

data class GeoLocation(
    @field:DecimalMin(value = "-90.0") @field:DecimalMax(value = "90.0") @field:Digits(integer = 2, fraction = 8) val lat: BigDecimal,
    @field:DecimalMin(value = "-180.0") @field:DecimalMax(value = "180.0") @field:Digits(integer = 3, fraction = 8) val lon: BigDecimal
)

data class AdditionalGeoLocation(
    @field:DecimalMin(value = "-90.0") @field:DecimalMax(value = "90.0") @field:Digits(integer = 2, fraction = 8) val lat: BigDecimal,
    @field:DecimalMin(value = "-180.0") @field:DecimalMax(value = "180.0") @field:Digits(integer = 3, fraction = 8) val lon: BigDecimal,
    val name: DisplayText? = null
)

data class DisplayText(
    @field:Size(min = 2, max = 2) val language: String,
    @field:Size(min = 1, max = 512) val text: String
)

data class Image(
    val url: String,
    val thumbnail: String? = null,
    val category: ImageCategory,
    @field:Size(min = 1, max = 4) val type: String,
    val width: Int? = null,
    val height: Int? = null
)

data class BusinessDetails(
    val id: String,
    @field:Size(min = 2, max = 100) val name: String,
    val website: String? = null,
    @field:Valid val logo: Image? = null
)

data class Hours(
    @field:JsonProperty("twentyfourseven") val twentyFourSeven: Boolean,
    @field:JsonProperty("regular_hours") @field:Valid val regularHours: List<RegularHours>? = emptyList(),
    @field:JsonProperty("exceptional_openings") @field:Valid val exceptionalOpenings: List<ExceptionalPeriod>? = emptyList(),
    @field:JsonProperty("exceptional_closings") @field:Valid val exceptionalClosings: List<ExceptionalPeriod>? = emptyList()
)

data class RegularHours(
    @field:Min(1) @field:Max(7) val weekday: Int,
    @field:JsonProperty("period_begin") @field:Pattern(regexp = "[0-2][0-9]:[0-5][0-9]") val periodBegin: String,
    @field:JsonProperty("period_end") @field:Pattern(regexp = "[0-2][0-9]:[0-5][0-9]") val periodEnd: String
)

data class ExceptionalPeriod(
    @field:JsonProperty("period_begin") val periodBegin: String,
    @field:JsonProperty("period_end") val periodEnd: String
)

data class EnergyMix(
    @JsonProperty("is_green_energy") @field:JsonProperty("is_green_energy") val isGreenEnergy: Boolean,
    @field:JsonProperty("energy_sources") val energySources: List<EnergySource>? = emptyList(),
    @field:JsonProperty("environ_impact") val environImpact: List<EnvironmentalImpact>? = emptyList(),
    @field:JsonProperty("supplier_name") @field:Size(min = 2, max = 64) val supplierName: String? = null,
    @field:JsonProperty("energy_product_name") @field:Size(min = 2, max = 64) val energyProductName: String? = null
)

data class EnergySource(
    val source: EnergySourceCategory,
    val percentage: BigDecimal
)

data class EnvironmentalImpact(
    val category: EnvironmentalImpactCategory,
    val amount: BigDecimal
)

data class StatusSchedule(
    @field:JsonProperty("period_begin") val periodBegin: Instant,
    @field:JsonProperty("period_end") val periodEnd: Instant? = null,
    val status: Status
)
